<%@page import="DAO.Group"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Welcome screen</title>
</head>
<body bgcolor=#A7A9C2>
<table style="background-color:#8489C2;" width =100%>
<tr>
<td width=40% ailgn="right">
	<h2> Awesome Social Network 1.4  </h2>
</td> 
<td width = 50% align="right">
	<span>
	<%
		DAO.User user =  ((DAO.User) session.getAttribute("user"));
		out.print(user.getName());
		out.print(" " + user.getSurname());
	%>
	</span>
</td>
<td width = 10% align="left">
	<form method = POST action = "/SocialNetwork/logout">
		<input type="submit"  value="Log Out"/>
	</form>
</td>
</tr>
</table>	
	
<div style="width:100%; height:auto; background:#6671ED;"> 
<table width=100%> 

<tr>
 	
	<td width=30%>
		<table width = 100% style="border-right:2px dashed white;">
			<tr>
			<td width = 100%"><a href = "/SocialNetwork/groups"> Groups </a></td>
			</tr>
			<tr>
			<td width = 100%"> Settings </td>
			</tr>
			<tr>
			<td width = 100%"> My Account </td>
			</tr>
		</table>
	</td>
	<td width=70% >
		<table width="70%" >
		<%
			java.util.ArrayList<DAO.Group> groups = (java.util.ArrayList<DAO.Group>) request.getAttribute("groups");
			for (DAO.Group group: groups) {
				out.println("<tr><td>" + 
					"<form action=\"/SocialNetwork/showgroup\" method=\"post\">" +
					"<a href=\"javascript:;\" onclick=\"parentNode.submit();\">" + group.getName()  + "</a>"
					+ "<input type=\"hidden\" name=\"id\" value=\"" + group.getId() +  "\"/></form>"
					+ "</td><td>" + group.getDescription() 
					+ "</td></tr>");
			}
		%>
		</table>
	</td>
 	
</tr>

</table>
</div> 
	<h3>
	You can create some new group in here:
	</h3>
	<form method = POST action = "/SocialNetwork/creategroup">
		<input type="text" 		name="name" placeholder="Your group name :)"/> <br/>
		<input type="text" name="description" placeholder="Some group description here..."/>
		<br/>
		<input type="submit" value="Create Group"/>
	</form>
</body>
</html>