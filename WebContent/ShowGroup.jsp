<%@page import="DAO.Group"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Welcome screen</title>
</head>
<body bgcolor=#A7A9C2>
<table style="background-color:#8489C2;" width =100%>
<tr>
<td width=40% ailgn="right">
	<h2> Awesome Social Network 1.4  </h2>
</td> 
<td width = 50% align="right">
	<span>
	<%
		DAO.User user =  ((DAO.User) session.getAttribute("user"));
		out.print(user.getName());
		out.print(" " + user.getSurname());
	%>
	</span>
</td>
<td width = 10% align="left">
	<form method = POST action = "/SocialNetwork/logout">
		<input type="submit"  value="Log Out"/>
	</form>
</td>
</tr>
</table>	
	
<div style="width:100%; height:auto; background:#6671ED;"> 
<table width=100%> 

<tr>
 	
	<td width=30%>
		<table width = 100% style="border-right:2px dashed white;">
			<tr>
			<td width = 100%"><a href = "/SocialNetwork/groups"> Groups </a></td>
			</tr>
			<tr>
			<td width = 100%"> Settings </td>
			</tr>
			<tr>
			<td width = 100%"> My Account </td>
			</tr>
		</table>
	</td>
	<td width=70% >
		<table width="70%" >
		<%
			DAO.Group group = (DAO.Group) request.getAttribute("group");
			out.write("<tr><td><h1>" + group.getName() + "</h1></td></tr>");
			out.write("<tr><td>" + group.getDescription() + "</td></tr>");
		%>
		</table>
	</td>
 	
</tr>
</table>
</div> 

<center><h2>Messages</h2></center>

 <form method="POST" action="/SocialNetwork/addmessage">
 <textarea rows="6" cols="50" name="message">Write something here</textarea>
 <% out.write("<input type=\"hidden\" name=\"id\" value=\""+ group.getId() + "\"/>\n"); %>
 <input type="submit" value="Write Message"/>
 </form>
<center>
<table width=80% border = 1>
<tr><td width="20%"> <b>User</b> </td> <td width=80%"> <b>Message</b> </td> </tr>
<% 
	java.util.ArrayList<DAO.Message> messages = (java.util.ArrayList<DAO.Message>) request.getAttribute("messages");
	if (messages != null)  {
		for (DAO.Message message: messages) {
			out.write("<tr><td>" + message.getUser().getName() + " " + message.getUser().getSurname() + "</td>");
			out.write("<td>" + message.getText() + "</td></tr>");
		}
	}
%>
</table>
</center>
</body>
</html>