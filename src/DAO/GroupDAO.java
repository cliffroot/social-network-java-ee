package DAO;

import java.util.ArrayList;
import java.util.List;

public interface GroupDAO {

	public Group createGroup (Group group);
	
	public List<Group> getAllGroups ();
	
	public Group getGrouById(long id);
	
	public ArrayList<Message> getAllMessages (long groupId);
	
}
