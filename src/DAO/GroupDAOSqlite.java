package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class GroupDAOSqlite implements GroupDAO {
	private Connection connection;
	
	private void openConnection() {
	    try {
	    	connection = DriverManager.getConnection("jdbc:sqlite:/Users/cliffroot/Documents/workspace/SocialNetwork/social.db");
	    } catch ( Exception e ) {
		    e.printStackTrace();
	    }
	}
	
	private void closeConnection ()  {
		try {
			connection.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Group createGroup(Group group) {
		try {
			openConnection();
		    String sql = "INSERT INTO COMMUNITY (NAME,DESCRIPTION) " + "VALUES (?,?);"; 
		    PreparedStatement createGroupQuery = connection.prepareStatement(sql);
		    createGroupQuery.setString(1, group.getName());
		    createGroupQuery.setString(2, group.getDescription());
		    createGroupQuery.execute();
		    closeConnection();
		    return group;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Group> getAllGroups() {
		try {
			openConnection();
		    String sql = "SELECT * FROM COMMUNITY"; 
		    PreparedStatement createGroupQuery = connection.prepareStatement(sql);
		    ArrayList <Group> groups = new ArrayList<Group> ();
		    ResultSet rs = createGroupQuery.executeQuery();
		    while (rs.next()) {
		    	long id = rs.getLong(1);
		    	String name = rs.getString(2);
		    	String description = rs.getString(3);
		    	groups.add(new Group(id, name, description));
		    }
		    closeConnection();
		    return groups;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Group getGrouById(long id) {
		try {
			openConnection();
		    String sql = "SELECT * FROM COMMUNITY where id=?"; 
		    PreparedStatement createGroupQuery = connection.prepareStatement(sql);
		    createGroupQuery.setLong(1, id);
		    Group group = null;
		    ResultSet rs = createGroupQuery.executeQuery();
		    if (rs.next()) {
		    	long uid = rs.getLong(1);
		    	String name = rs.getString(2);
		    	String description = rs.getString(3);
		    	group = new Group(uid, name, description);
		    }
		    closeConnection();
		    return group;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<Message> getAllMessages(long groupId) {
		try {
			openConnection();
		    String sql = "select * from message, messageincommunity where message.id=messageincommunity.messageid and messageincommunity.groupid=?"; 
		    PreparedStatement createGroupQuery = connection.prepareStatement(sql);
		    createGroupQuery.setLong(1, groupId);
		    ArrayList <Message>messages = new ArrayList<Message>(); 
		    ResultSet rs = createGroupQuery.executeQuery();
		    while (rs.next()) {
		    	long uid = rs.getLong(1);
		    	String text = rs.getString(2);
		    	String date = rs.getString(3);
		    	long userId = rs.getLong(4);
		    	//closeConnection();
		    	User messageOwner = new UserDAOSqlite().findUserById(userId);
		    	//openConnection();
		    	System.out.println("This message " + uid + " belongs to " + messageOwner.getName() + " " + messageOwner.getSurname());
		    	messages.add(new Message(uid, messageOwner, text, date));
		    }
		    //closeConnection();
		    return messages;
		    //return group;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
