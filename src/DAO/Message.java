package DAO;
public class Message {
	private User user;
	private String text, date;
	private long id;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Message () { }
	
	public Message (long id, User user, String text, String date) {
		this.id   =   id;
		this.user = user;
		this.text = text;
		this.date = date;
	}
	
	public Message (User user, String text, String date) {
		this.user = user;
		this.text = text;
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	
}
