package DAO;

import java.util.List;

public interface MessageDAO {

	public Message createMessage (Message message, long groupId);
	
	//public User getMessageOwner (Message message);
	
	public Message editMessage (Message message);
	
	public Message findMessageById (long ID);
	
	public List<Message> getAllUserMessages (long UserID);
}
