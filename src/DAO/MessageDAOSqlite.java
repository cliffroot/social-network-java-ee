package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MessageDAOSqlite implements MessageDAO {
	private Connection connection;
	
	private void openConnection() {
	    try {
	    	connection = DriverManager.getConnection("jdbc:sqlite:/Users/cliffroot/Documents/workspace/SocialNetwork/social.db");
	    } catch ( Exception e ) {
		    e.printStackTrace();
	    }
	}
	
	private void closeConnection ()  {
		try {
			connection.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Message createMessage(Message message, long groupId) {
		try {
			openConnection();
		    String sql = "INSERT INTO MESSAGE (TEXT,DATE,USERID) " + "VALUES (?, ?, ?);"; 
		    PreparedStatement query = connection.prepareStatement(sql);
		    query.setString(1, message.getText());
		    query.setString(2, message.getDate());
		    query.setLong(3, message.getUser().getID());
		    query.executeUpdate();
		    closeConnection();
		    message = findMessage(message.getText(), message.getDate(), message.getUser());
		    openConnection(); // not sure what i'm doing;
		    sql = "INSERT INTO MessageInCommunity (messageId, groupId) " + "VALUES (?, ?)";
		    query = connection.prepareStatement(sql);
		    query.setLong(1, message.getId());
		    query.setLong(2, groupId);
		    query.executeUpdate();
		    closeConnection();
		    return message;
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	
	//brutal hack, i guess we can use it though
	private Message findMessage (String text, String date, User user) {
		try {
			openConnection();
		    String sql = "SELECT * FROM MESSAGE WHERE TEXT=? and DATE=? and UserId=?"; 
		    PreparedStatement query = connection.prepareStatement(sql);
		    query.setString(1, text);
		    query.setString(2, date);
		    query.setLong(3, user.getID());
		    ResultSet rs = query.executeQuery();
			if ( rs.next() ) {
			    long id = rs.getLong("id");
			    closeConnection();
				return new Message(id,  user, text, date);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return null;
	}

	@Override
	public Message editMessage(Message message) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Message findMessageById(long ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Message> getAllUserMessages(long UserID) {
		// TODO Auto-generated method stub
		return null;
	}

}
