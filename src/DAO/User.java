package DAO;
public class User {
	private long ID;
	private String name, surname;
	private boolean authorized = false;
	private String password;
	
	public User (String name, String surname, String password) {
		this.name = name;
		this.surname = surname;
		this.password = password;
	}
	
	public User(long ID, String name, String surname, String password) {
		this.ID = ID;
		this.name = name;
		this.surname = surname; 
		this.password = password;
	}
	
	public boolean isAuthorized() {
		return authorized;
	}
	
	public void setAuthorized (boolean value) {
		this.authorized = value;
	}
	
	public User () {	}

	public String getPassword () {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
}
