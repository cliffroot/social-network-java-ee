package DAO;

import java.util.List;

public interface UserDAO {

	public User createUser (User user);
	
	public User findUserById (long ID);
	
	public User updateUser (User user);
	
	public void deleteUser (long ID);
	
	public List<User> getAllUsers ();
	
	public User validUserCredentials (String name, String password);
}
