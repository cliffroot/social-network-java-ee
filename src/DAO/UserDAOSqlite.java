package DAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDAOSqlite implements UserDAO {
	private Connection connection;
	
	private void openConnection() {
	    try {
	    	connection = DriverManager.getConnection("jdbc:sqlite:/Users/cliffroot/Documents/workspace/SocialNetwork/social.db");
	    } catch ( Exception e ) {
		    e.printStackTrace();
	    }
	}
	
	private void closeConnection ()  {
		try {
			connection.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public UserDAOSqlite() {}

	@Override
	public User createUser(User user) {
		try {
			openConnection();
		    String sql = "INSERT INTO USER (NAME,SURNAME,PASSWORD) " + "VALUES (?, ?, ?);"; 
		    PreparedStatement addUserQuery = connection.prepareStatement(sql);
		    addUserQuery.setString(1, user.getName());
		    addUserQuery.setString(2, user.getSurname());
		    addUserQuery.setString(3, user.getPassword());
		    addUserQuery.executeUpdate();
		    closeConnection();
		    return user;
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return null;
	}

	@Override
	public User findUserById(long ID) {
		try {
			openConnection();
			String sql = "SELECT * FROM USER WHERE ID = ?";
			PreparedStatement createUserQuery = connection.prepareStatement(sql);
			createUserQuery.setLong(1, ID);
			ResultSet rs = createUserQuery.executeQuery();
			if (rs.next()) {
			    long id = ID;
				String  name = rs.getString("name");
				String surname  = rs.getString("surname");
				String password = rs.getString("password");
				closeConnection();
				return new User(id, name, surname, password);
			}
		}
		catch (Exception ex) {};
		return null;
	}

	@Override
	public User updateUser(User user) {
		try {
			openConnection();
		    String sql = "UPDATE USER set name = ?, surname = ?, password = ? where ID = ?;";
		    PreparedStatement updateUserQuery = connection.prepareStatement(sql);
		    updateUserQuery.setString(1, user.getName());
		    updateUserQuery.setString(2, user.getSurname());
		    updateUserQuery.setString(3, user.getPassword());
		    updateUserQuery.setLong(4, user.getID());
		    updateUserQuery.executeUpdate();
		    connection.commit();
		    closeConnection();
		    return user;
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	

	@Override
	public void deleteUser(long ID) {
		try {
			openConnection();
		    String sql = "DELETE FROM USER WHERE ID = ?"; 
		    PreparedStatement deleteUserQuery = connection.prepareStatement(sql);
		    deleteUserQuery.setLong(1, ID);
		    deleteUserQuery.executeUpdate();
		    closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	public List<User> getAllUsers () {
		List <User> res = new ArrayList <User> ();
		try {
			openConnection();
			String sql = "SELECT * FROM USER;";
			PreparedStatement createUserQuery = connection.prepareStatement(sql);
			ResultSet rs = createUserQuery.executeQuery();
			while ( rs.next() ) {
			    int id = rs.getInt("id");
				String  name = rs.getString("name");
				String surname  = rs.getString("surname");
				String password = rs.getString("password");
				res.add(new User(id, name, surname, password));
			}
			closeConnection();
		}
		catch (Exception ex) {};
		return res;
	}

	@Override
	public User validUserCredentials(String name, String password) {
		try {
			openConnection();
			String sql = "SELECT * FROM USER WHERE NAME = ? AND PASSWORD = ?";
			PreparedStatement checkUserQuery = connection.prepareStatement(sql);
			checkUserQuery.setString(1, name);
			checkUserQuery.setString(2, password);
			ResultSet rs = checkUserQuery.executeQuery();
			if ( rs.next() ) {
				int id = rs.getInt("id");
				String uname = rs.getString("name");
				String surname = rs.getString("surname");
				String upassword = rs.getString("password");
				closeConnection();
				return new User(id, uname, surname, upassword);
			}
			else throw new RuntimeException("Ololo");
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
