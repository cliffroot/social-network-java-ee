package servlets;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {
	
	static HashMap <String, Action> mapping = new HashMap<String, Action>();
	
	static {
		mapping.put("welcome", new WelcomeAction());
		mapping.put("login", new LoginAction());
		mapping.put("logout", new LogoutAction());
		mapping.put("changepassword", new ChangePasswordAction());
		mapping.put("creategroup", new CreateGroupAction());
		mapping.put("groups", new DisplayGroupAction());
		mapping.put("showgroup", new ShowGroupAction());
		mapping.put("addmessage", new AddMessageAction());
		mapping.put("register", new RegisterAction());
		mapping.put("signup", new SignUpAction());
	}
	
	public static Action getAction(HttpServletRequest request) {
		String url = request.getRequestURL().toString();
		String[] tokens  = url.split("/");
		String lastToken = tokens[tokens.length - 1];
		System.out.println("Path: " + lastToken);
		return mapping.get(lastToken);
	}
}
