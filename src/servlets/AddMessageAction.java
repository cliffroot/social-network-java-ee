package servlets;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import DAO.Message;
import DAO.MessageDAO;
import DAO.MessageDAOSqlite;
import DAO.User;

public class AddMessageAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		System.out.println(request.getParameter("id"));
		long groupId = Long.parseLong(request.getParameter("id"));
		String text = request.getParameter("message");
		Calendar calendar  = Calendar.getInstance();
		String strdate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:SS");
		strdate = sdf.format(calendar.getTime());
		User user = (User)request.getSession().getAttribute("user");
		
		MessageDAO messageDao = new MessageDAOSqlite();
		messageDao.createMessage(new Message(user, text, strdate), groupId);
		return new ShowGroupAction().execute(request);
	}

}
