package servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import DAO.User;
import DAO.UserDAO;
import DAO.UserDAOSqlite;

public class ChangePasswordAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		HttpSession httpSession = request.getSession();
		User user = ((User) httpSession.getAttribute("user"));
		UserDAO userDao = new UserDAOSqlite();
		String oldPassword = request.getParameter("oldPassword");
		String newPassword = request.getParameter("newPassword");
		user = userDao.validUserCredentials(user.getName(), oldPassword);
		if (user != null) {
			user.setPassword(newPassword);
			userDao.updateUser(user);
		}
		return "Welcome.jsp";
	}

}
