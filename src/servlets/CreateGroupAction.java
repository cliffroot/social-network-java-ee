package servlets;

import javax.servlet.http.HttpServletRequest;

import DAO.Group;
import DAO.GroupDAO;
import DAO.GroupDAOSqlite;

public class CreateGroupAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		GroupDAO groupDao = new GroupDAOSqlite();
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		groupDao.createGroup(new Group(name, description));
		
		return new DisplayGroupAction().execute(request);
	}

}
