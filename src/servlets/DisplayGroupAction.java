package servlets;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import DAO.Group;
import DAO.GroupDAO;
import DAO.GroupDAOSqlite;

public class DisplayGroupAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		GroupDAO groupDao = new GroupDAOSqlite();
		ArrayList<Group> groups = (ArrayList<Group>) groupDao.getAllGroups();
		request.setAttribute("groups", groups);
		return "Groups.jsp";
	}

}
