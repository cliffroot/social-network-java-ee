package servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import DAO.User;
import DAO.UserDAO;
import DAO.UserDAOSqlite;

public class LoginAction implements Action{

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getSession().getAttribute("user") == null) {
			String login    = request.getParameter("Login");
			String password = request.getParameter("Password");
			UserDAO userDao = new UserDAOSqlite();
			User user = userDao.validUserCredentials(login, password);
			System.out.println(login);
			System.out.println(password);
			HttpSession httpSession = request.getSession();
			if (user != null) {
				user.setAuthorized(true);
				httpSession.setAttribute("user", user );
				return "Welcome.jsp";
			}
			else {
				return "LogIn.jsp";
			}
		}
		else 
			return "Welcome.jsp";
	}
	
}
