package servlets;

import javax.servlet.http.HttpServletRequest;

public class LogoutAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		
		request.getSession().removeAttribute("user");
		return "LogIn.jsp";
	}

}
