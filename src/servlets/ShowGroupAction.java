package servlets;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import DAO.Group;
import DAO.GroupDAO;
import DAO.GroupDAOSqlite;
import DAO.Message;

public class ShowGroupAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		long id= Long.parseLong(request.getParameter("id"));
		GroupDAO groupDao = new GroupDAOSqlite();
		Group group = groupDao.getGrouById(id);
		ArrayList<Message> messages = groupDao.getAllMessages(id);
		request.setAttribute("group", group);
		request.setAttribute("messages", messages);
		
		/*
		System.out.println("Group id: " + id);
		System.out.println("Group: "+ group.getDescription());
		System.out.println("Show group: " + group);
		*/
		System.out.println("Show group messages: " + messages);
	
		return "ShowGroup.jsp";
	}

}
