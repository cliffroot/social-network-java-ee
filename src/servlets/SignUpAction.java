package servlets;

import javax.servlet.http.HttpServletRequest;

import DAO.User;
import DAO.UserDAO;
import DAO.UserDAOSqlite;

public class SignUpAction implements Action {

	@Override
	public String execute(HttpServletRequest request) {
		String name     = request.getParameter("Name");
		String password = request.getParameter("Password");
		String surname  = request.getParameter("Surname");
		
		UserDAO userDao = new UserDAOSqlite();
		userDao.createUser(new User(name, surname, password));
		return "LogIn.jsp";
	}

}
