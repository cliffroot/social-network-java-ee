package servlets;

import javax.servlet.http.HttpServletRequest;

public class WelcomeAction implements Action{

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getSession().getAttribute("user") == null)
			return "LogIn.jsp";
		else 
			return "Welcome.jsp";
	}

}
